//
//  NSString+HTMLEscaping.h
//  WebKIT
//
//  Created by Cory Sullivan on 2014-12-26.
//  Copyright (c) 2014 Cory Sullivan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (HTMLEscaping)

- (NSString *)escapeHTML;

@end
