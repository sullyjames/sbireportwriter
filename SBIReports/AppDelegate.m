//
//  AppDelegate.m
//  SBIReports
//
//  Created by Cory Sullivan on 2014-12-29.
//  Copyright (c) 2014 Cory Sullivan. All rights reserved.
//

#import "AppDelegate.h"
#import "SBIReportWriter.h"

@interface AppDelegate ()<SBIHTMLWriterDataSource>

@property (weak) IBOutlet NSWindow *window;
@property (nonatomic, copy) NSArray *dataSource;
@property (weak) IBOutlet SBIReportWriter *writer;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    self.dataSource = @[@"Truck", @"Car", @"Van" ,@"Bike", @"Scooter"];
    NSURL *css = [[NSBundle mainBundle] URLForResource:@"SBIStyle" withExtension:@"css"];
    self.writer.cssLocation = css;
    [self.writer reloadHTMLData];
}


#pragma mark - Report Writer Protocol

- (NSInteger)numberOfRowsInHTMLTable {
    
    return 100;
}

- (NSInteger)numberOfColumnsInHTMLTable {
    
    return 3;
}

- (NSString *)stringValueForRow:(NSInteger)rowIndex tableColumn:(NSInteger)aTableColumn {
    
    return @"Hello";
}
- (NSString *)reportTitle
{
    return @"Sullivan Family";
}

- (NSString *)columnHeaderForColumn:(NSInteger)aTableColumn {
    switch (aTableColumn) {
        case 0:
            return @"People";
            break;
        case 1:
            return @"Children";
            break;
        case 2:
            return @"Three";
            break;
            
        default:
            return @"default";
            break;
    }
}

- (NSString *)footerForColumn:(NSInteger)aTableColumn {
    return @"TOTAL";
}


@end
