//
//  AppDelegate.h
//  SBIReports
//
//  Created by Cory Sullivan on 2014-12-29.
//  Copyright (c) 2014 Cory Sullivan. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

