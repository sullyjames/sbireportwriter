//
//  SBIPrinting.h
//  WebKIT
//
//  Created by Cory Sullivan on 2014-12-27.
//  Copyright (c) 2014 Cory Sullivan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Quartz/Quartz.h>
#import <WebKit/WebKit.h>

@interface SBIPrinting : NSObject

- (instancetype)init:(WebView *)webView
         withPDFView:(PDFView *)pdfView;

@end
