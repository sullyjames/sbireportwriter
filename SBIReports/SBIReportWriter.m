//
//  SBIHTMLWriter.m
//  WebKIT
//
//  Created by Cory Sullivan on 2014-12-14.
//  Copyright (c) 2014 Cory Sullivan. All rights reserved.
//
#import "NSString+HTMLEscaping.h"
#import "SBIReportWriter.h"
#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>
#import "SBIPrinting.h"

@interface SBIReportWriter () {
    struct {
        unsigned int numberOfRows       : 1;
        unsigned int StringValueForRow  : 1;
        unsigned int numberOfColumns    : 1;
        unsigned int columnTitles       : 1;
        unsigned int reportTitle        : 1;
        unsigned int columnFooter       : 1;
    } _dataSourceFlags;
}

@property (nonatomic, strong) NSMutableString *outputFile;
@property (nonatomic, strong) WebView *webView;
@property (nonatomic, strong) SBIPrinting *printDelegate;
@end

@implementation SBIReportWriter


- (void)awakeFromNib {
    if (!self.outputFile) {
        self.outputFile = [[NSMutableString alloc] init];
    }
    [self configureWebView];
}

#pragma mark - SBIHTMLWriterDataSource Protocol

- (void)setDataSource:(id<SBIHTMLWriterDataSource>)dataSource {
    _dataSource = dataSource;
    _dataSourceFlags.numberOfRows = [dataSource respondsToSelector:@selector(numberOfRowsInHTMLTable)];
    _dataSourceFlags.numberOfColumns = [dataSource respondsToSelector:@selector(numberOfColumnsInHTMLTable)];
    _dataSourceFlags.columnTitles = [dataSource respondsToSelector:@selector(columnHeaderForColumn:)];
    _dataSourceFlags.reportTitle = [dataSource respondsToSelector:@selector(reportTitle)];
    _dataSourceFlags.StringValueForRow = [dataSource respondsToSelector:@selector(stringValueForRow:tableColumn:)];
    _dataSourceFlags.columnFooter = [dataSource respondsToSelector:@selector(footerForColumn:)];
}

- (NSInteger)numberOfRowsInHTMLTable{
    if (_dataSourceFlags.numberOfRows) {
        return [_dataSource numberOfRowsInHTMLTable];
    }
    return 0;
}
- (NSInteger)numberOfColumnsInHTMLTable {
    if (_dataSourceFlags.numberOfColumns) {
        return [_dataSource numberOfColumnsInHTMLTable];
    }
    return 0;
}

- (NSString *)stringValueForRow:(NSInteger)rowIndex
                    tableColumn:(NSInteger)aTableColumn{
    if (_dataSourceFlags.StringValueForRow) {
        return [[_dataSource stringValueForRow:rowIndex
                                  tableColumn:aTableColumn] escapeHTML];
    }
    return @"";
}

- (NSString *)reportTitle {
    if (_dataSourceFlags.reportTitle) {
        return [[_dataSource reportTitle] escapeHTML];
    }
    return @"";
}
- (NSString *)columnHeaderForColumn:(NSInteger)aTableColumn{
    if (_dataSourceFlags.columnTitles) {
        return [[_dataSource columnHeaderForColumn:aTableColumn] escapeHTML];
    }
    return @"";
}

- (NSString *)footerForColumn:(NSInteger)aTableColumn {
    if (_dataSourceFlags.columnFooter) {
        return [[_dataSource footerForColumn:aTableColumn] escapeHTML];
    }
    return @"";
}

- (void)reloadHTMLData
{
    [self.outputFile setString:@""]; //Clear String
    [self.outputFile appendString:@"<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\">"];
    [self.outputFile appendString:[self openElement:@"html"]];
    [self writeElement:@"head" text:@""];

    [self writeElement:@"h1" text:[self reportTitle]];
    [self.outputFile appendString:[self openElement:@"body"]];
    //Create Data for Table
    [self.outputFile appendString:[self openElement:@"TABLE"]];
    //Configure data for column headers
    [self.outputFile appendString:[self openElement:@"thead"]];
    for (int col = 0; col < [self numberOfColumnsInHTMLTable]; col++) {
        [self.outputFile appendString:[self openElement:@"th"]];
        [self.outputFile appendString:[self columnHeaderForColumn:col]];
        [self.outputFile appendString:[self closeElement:@"th"]];
    }
    [self.outputFile appendString:[self closeElement:@"thead"]];
    
    [self.outputFile appendString:[self openElement:@"tbody"]];
    for (int row = 0; row < [self numberOfRowsInHTMLTable]; row++) {
        [self.outputFile appendString:[self openElement:@"TR"]];
        for (int col = 0; col < [self numberOfColumnsInHTMLTable]; col++) {
            [self.outputFile appendString:[self openElement:@"TD"]];
            [self.outputFile appendString:[self stringValueForRow:row tableColumn:col]];
            [self.outputFile appendString:[self closeElement:@"TD"]];
        }
        [self.outputFile appendString:[self closeElement:@"TR"]];
    }
    [self.outputFile appendString:[self closeElement:@"tbody"]];
    [self.outputFile appendString:[self openElement:@"tfoot"]];
    [self.outputFile appendString:[self openElement:@"tr"]];
    for (int col = 0; col < [self numberOfColumnsInHTMLTable]; col++) {
        [self.outputFile appendString:[self openElement:@"td"]];
        [self.outputFile appendString:[self footerForColumn:col]];
        [self.outputFile appendString:[self closeElement:@"td"]];
    }
    [self.outputFile appendString:[self closeElement:@"tr"]];
    [self.outputFile appendString:[self closeElement:@"tfoot"]];
    [self.outputFile appendString:[self closeElement:@"TABLE"]];
    [self.outputFile appendString:[self closeElement:@"body"]];
    [self.outputFile appendString:[self closeElement:@"html"]];
    
    //Update Webview
    [[self.webView mainFrame] loadHTMLString:self.outputFile baseURL:nil];
}


#pragma mark - Convience Methods

- (void)writeElement:(NSString *)elementName text:(NSString *)text {
    [self.outputFile appendString:[self openElement:elementName]];
    [self.outputFile appendString:text];
    [self.outputFile appendString:[self closeElement:elementName]];
}

- (NSString *)openElement:(NSString *)tagName {
    return [NSString stringWithFormat:@"<%@>", tagName];
}

- (NSString *)closeElement:(NSString *)tagName {
    return [NSString stringWithFormat:@"</%@>", tagName];
}

#pragma mark - WebView

- (void)configureWebView {
    if (!self.webView) {
        self.webView = [[WebView alloc] initWithFrame:NSZeroRect];
    }
    if (!self.printDelegate) {
        self.printDelegate = [[SBIPrinting alloc] init:self.webView
                                           withPDFView:self.pdfView];
    }
    self.webView.frameLoadDelegate = self.printDelegate;
    self.webView.UIDelegate = self.printDelegate;
    if (self.cssLocation) {
        self.webView.preferences.userStyleSheetEnabled = YES;
        self.webView.preferences.userStyleSheetLocation = self.cssLocation;
    }
}


@end
