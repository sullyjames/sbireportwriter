//
//  SBIPrinting.m
//  WebKIT
//
//  Created by Cory Sullivan on 2014-12-27.
//  Copyright (c) 2014 Cory Sullivan. All rights reserved.
//

#import "SBIPrinting.h"

@interface SBIPrinting ()

@property (nonatomic, strong) WebView *webView;
@property (nonatomic, strong) PDFView *pdfView;

@end

@implementation SBIPrinting

- (instancetype)init:(WebView *)webView
         withPDFView:(PDFView *)pdfView {
    
    self = [super init];
    if (self) {
        _webView = webView;
        _pdfView = pdfView;
    }
    return self;
}

- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame {
    
    NSView *docView = [[[self.webView mainFrame] frameView] documentView];
    
    NSFileManager *fm = [[NSFileManager alloc] init];
    NSArray *urls = [fm URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask];
    NSURL *appSupportURL = [urls objectAtIndex:0];
    //TODO:check if directory exists
    NSURL *tempFile = [appSupportURL URLByAppendingPathComponent:@"Allocator/.tempfile"];
    
    NSMutableDictionary *info = [[NSPrintInfo sharedPrintInfo] dictionary];
    [info setObject:NSPrintSaveJob forKey:NSPrintJobDisposition];
    [info setObject:tempFile forKey:NSPrintJobSavingURL];
    [info setObject:@YES forKey:NSPrintJobSavingFileNameExtensionHidden];
    NSPrintInfo *printInfo = [[NSPrintInfo alloc] initWithDictionary:info];
    
    [printInfo setHorizontalPagination: NSFitPagination];
    [printInfo setVerticallyCentered:NO];
    [printInfo setHorizontallyCentered:NO];
    
    NSRect imageableBounds = [printInfo imageablePageBounds];
    NSSize paperSize = [printInfo paperSize];
    if (NSWidth(imageableBounds) > paperSize.width) {
        imageableBounds.origin.x = 0;
        imageableBounds.size.width = paperSize.width;
    }
    if (NSHeight(imageableBounds) > paperSize.height) {
        imageableBounds.origin.y = 0;
        imageableBounds.size.height = paperSize.height;
    }
    
    [printInfo setBottomMargin:NSMinY(imageableBounds) + 10.0f];
    [printInfo setTopMargin:paperSize.height - NSMinY(imageableBounds) - NSHeight(imageableBounds) + 10.0f];
    [printInfo setLeftMargin:NSMinX(imageableBounds) + 10.0f];
    [printInfo setRightMargin:paperSize.width - NSMinX(imageableBounds) - NSWidth(imageableBounds) + 10.0f];
    
    NSPrintOperation *op = [NSPrintOperation printOperationWithView:docView printInfo:printInfo];
    [op setShowsPrintPanel:NO];
    [op setShowsProgressPanel:NO];
    
    if ([op runOperation]){
        PDFDocument *doc = [[PDFDocument alloc] initWithURL:tempFile];
        [self.pdfView setDocument:doc];
        NSFileManager *manager = [NSFileManager defaultManager];
        [manager removeItemAtURL:tempFile error:nil];
    }
}


- (float)webViewFooterHeight:(WebView *)sender {
    return 35.0f;
}

- (void)webView:(WebView *)sender
drawFooterInRect:(NSRect)rect {
    
    //// footer Border
    NSBezierPath* bezierPath = NSBezierPath.bezierPath;
    [bezierPath moveToPoint: NSMakePoint(NSMinX(rect) + 5.0f, NSMaxY(rect) - 4.5)];
    [bezierPath lineToPoint: NSMakePoint(NSMinX(rect) + NSWidth(rect) - 5.0f, NSMaxY(rect) - 4.5)];
    [NSColor.blackColor setStroke];
    [bezierPath setLineWidth: 1];
    [bezierPath stroke];
    
    //// Text Drawing for Company Name
    NSRect textRect = NSMakeRect(NSMinX(rect) + 5, NSMinY(rect) + NSHeight(rect) - 26, NSWidth(rect) - 154, 22);
    {
        NSString* textContent = @"Solbits Software Inc.";
        NSMutableParagraphStyle* textStyle = NSMutableParagraphStyle.defaultParagraphStyle.mutableCopy;
        textStyle.alignment = NSLeftTextAlignment;
        
        NSDictionary* textFontAttributes = @{NSFontAttributeName: [NSFont systemFontOfSize: NSFont.smallSystemFontSize], NSForegroundColorAttributeName: NSColor.blackColor, NSParagraphStyleAttributeName: textStyle};
        
        CGFloat textTextHeight = NSHeight([textContent boundingRectWithSize: textRect.size options: NSStringDrawingUsesLineFragmentOrigin attributes: textFontAttributes]);
        NSRect textTextRect = NSMakeRect(NSMinX(textRect), NSMinY(textRect) + (NSHeight(textRect) - textTextHeight) / 2, NSWidth(textRect), textTextHeight);
        [NSGraphicsContext saveGraphicsState];
        NSRectClip(textRect);
        [textContent drawInRect: NSOffsetRect(textTextRect, 0, 0) withAttributes: textFontAttributes];
        [NSGraphicsContext restoreGraphicsState];
    }
    
    //// Text Drawing for Page number and Location
    NSRect text2Rect = NSMakeRect(NSMinX(rect) + 182, NSMinY(rect) + NSHeight(rect) - 26, NSWidth(rect) - 188, 22);
    {
        NSString* textContent = [self currentPage];
        NSMutableParagraphStyle* text2Style = NSMutableParagraphStyle.defaultParagraphStyle.mutableCopy;
        text2Style.alignment = NSRightTextAlignment;
        
        NSDictionary* text2FontAttributes = @{NSFontAttributeName: [NSFont systemFontOfSize: NSFont.smallSystemFontSize], NSForegroundColorAttributeName: NSColor.blackColor, NSParagraphStyleAttributeName: text2Style};
        
        CGFloat text2TextHeight = NSHeight([textContent boundingRectWithSize: text2Rect.size options: NSStringDrawingUsesLineFragmentOrigin attributes: text2FontAttributes]);
        NSRect text2TextRect = NSMakeRect(NSMinX(text2Rect), NSMinY(text2Rect) + (NSHeight(text2Rect) - text2TextHeight) / 2, NSWidth(text2Rect), text2TextHeight);
        [NSGraphicsContext saveGraphicsState];
        NSRectClip(text2Rect);
        [textContent drawInRect: NSOffsetRect(text2TextRect, 0, 0) withAttributes: text2FontAttributes];
        [NSGraphicsContext restoreGraphicsState];
    }
}

- (NSString *)currentPage {
    NSInteger pageNumber = [[NSPrintOperation currentOperation] currentPage];
    NSRange pageRange = [[NSPrintOperation currentOperation] pageRange];
    return [NSString stringWithFormat:@"Page %ld of %lu", (long)pageNumber, (unsigned long)pageRange.length];
}
@end
