//
//  NSString+HTMLEscaping.m
//  WebKIT
//
//  Created by Cory Sullivan on 2014-12-26.
//  Copyright (c) 2014 Cory Sullivan. All rights reserved.
//

#import "NSString+HTMLEscaping.h"

@implementation NSString (HTMLEscaping)

- (NSString *)escapeHTML {
    NSString *specialCharacterString = @"&<>";
    NSCharacterSet *specialCharacterSet = [NSCharacterSet
                                           characterSetWithCharactersInString:specialCharacterString];
    
    //If the string doesn't contain special chacters then return
    if (![self.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length) {
        return self;
    }
    NSString *escaped;
    escaped = [self stringByReplacingOccurrencesOfString:@"&" withString:@"&amp"];
    escaped = [escaped stringByReplacingOccurrencesOfString:@"<" withString:@"&lt"];
    escaped = [escaped stringByReplacingOccurrencesOfString:@">" withString:@"&gt"];
    return escaped;
}


@end
