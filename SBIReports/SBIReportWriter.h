//
//  SBIHTMLWriter.h
//  WebKIT
//
//  Created by Cory Sullivan on 2014-12-14.
//  Copyright (c) 2014 Cory Sullivan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Quartz/Quartz.h>

@protocol SBIHTMLWriterDataSource <NSObject>
@required
- (NSInteger)numberOfRowsInHTMLTable;
- (NSInteger)numberOfColumnsInHTMLTable;
- (NSString *)stringValueForRow:(NSInteger)rowIndex
                    tableColumn:(NSInteger)aTableColumn;

@optional
- (NSString *)reportTitle;
- (NSString *)columnHeaderForColumn:(NSInteger)aTableColumn;
- (NSString *)footerForColumn:(NSInteger)aTableColumn;
@end

@interface SBIReportWriter : NSObject

@property (nonatomic, weak) IBOutlet id <SBIHTMLWriterDataSource> dataSource;
@property (weak) IBOutlet PDFView *pdfView;
@property (nonatomic, strong) NSURL *cssLocation;

- (void)reloadHTMLData;

@end

